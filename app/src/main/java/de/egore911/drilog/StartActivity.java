/* StartActivity.java
 *
 * Copyright (c) 2015 Christoph Brill <egore911@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.egore911.drilog.util.Constants;
import de.egore911.drilog.util.HttpUtil;

public class StartActivity extends AppCompatActivity {

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler(Looper.getMainLooper());

    private void loadData() {
        String url = Constants.BASE_URL + "channels.php";
        executor.execute(() -> {
            String[] result;
            try {
                Pair<String, URL> json = HttpUtil.getPlainTextFromUrl(url);
                JSONArray jsonArray = new JSONArray(json.first);
                result = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    result[i] = jsonArray.getString(i);
                }
            } catch (JSONException | IOException e) {
                Log.e(getClass().getSimpleName(), e.getMessage(), e);
                return;
            }

            Pair<String[], Integer> ui = new Pair<>(result, 0);

            handler.post(() -> {
                String[] channels = ui.first;
                if (channels == null) {
                    channels = new String[]{"dri-devel", "radeon", "nouveau"};
                }
                Intent intent = new Intent(StartActivity.this, ShowActivity.class);
                intent.putExtra("channels", channels);
                StartActivity.this.startActivity(intent);
            });
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadData();
    }
}
