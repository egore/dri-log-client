/* ShowActivity.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import de.egore911.drilog.adapter.ShowDataAdapter;
import de.egore911.drilog.db.DbOpenHelper;
import de.egore911.drilog.db.WatchesDao;
import de.egore911.drilog.fragments.LogviewFragment;
import de.egore911.drilog.model.Comment;
import de.egore911.drilog.model.ListElement;
import de.egore911.drilog.util.Constants;
import de.egore911.drilog.util.DateUtil;
import de.egore911.drilog.util.DateUtil.SimpleDate;
import de.egore911.drilog.util.HttpUtil;

import javax.annotation.Nonnull;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class is used to display the whole log of a channel for a day.
 *
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.4
 */
public class ShowActivity extends AppCompatActivity {

    /**
     * The list of monitored channels
     */
    private List<String> channels;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private final SparseArray<LogviewFragment> fragments = new SparseArray<>();

    private int mPosition = 0;

    private SimpleDate mSelectedDate;
    private String mAnchor;
    private Menu mMenu;
    private List<String> mWatchList = new ArrayList<>(0);
    public WatchesDao mWatchesDao;

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler(Looper.getMainLooper());

    /**
     * Load the data from the DRI-log server.
     */
    private void loadData() {
        LogviewFragment fragment = getCurrentFragment();
        String url = Constants.BASE_URL +
                "json.php?channel=" +
                fragment.getArguments().getString("channel") +
                "&date=" +
                getDateString() +
                "&mode=show";
        executor.execute(() -> {
                String exceptionMessage_;
                String json_;
                try {
                    Pair<String, URL> result = HttpUtil.getPlainTextFromUrl(url);
                    json_ = result.first;
                    exceptionMessage_ = null;
                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), e.getMessage(), e);
                    json_ = null;
                    exceptionMessage_ = e.getClass().getSimpleName() + ": "
                            + e.getLocalizedMessage();
                }
                Pair<String, String> ui = new Pair<>(json_, exceptionMessage_);

                handler.post(() -> {
                    String json = ui.first;
                    String exceptionMessage = ui.second;
                    ShowDataAdapter result;

                    JSONObject o = null;
                    try {
                        if (json != null) {
                            o = new JSONObject(json);
                        }
                    } catch (JSONException e) {
                        Log.e(getClass().getSimpleName(), e.getMessage(), e);
                        exceptionMessage = e.getClass().getSimpleName() + ": "
                                + e.getLocalizedMessage();
                    }

                    result = (ShowDataAdapter) fragment.getListAdapter();
                    var watchList = new HashSet<>(mWatchList);
                    if (result == null) {
                        result = new ShowDataAdapter(ShowActivity.this, o, watchList);
                    } else {
                        result.updateElements(o, watchList);
                    }

                    if (exceptionMessage != null) {
                        Toast.makeText(ShowActivity.this, exceptionMessage, Toast.LENGTH_LONG).show();
                    }
                    ShowActivity.this.setTitle(result.channel);
                    ShowActivity.this.updateDateText(DateUtil.getDateString(result.date == null ? new Date() : result.date));
                    ListView listView = null;
                    int oldposition;
                    try {
                        listView = fragment.getListView();
                        oldposition = listView.getFirstVisiblePosition();
                    } catch (IllegalStateException e) {
                        oldposition = -1;
                    }
                    fragment.setListAdapter(result);

                    // Now do some scrolling
                    String anchor = ShowActivity.this.getAnchor();
                    ShowActivity.this.setAnchor(null);
                    if (anchor != null) {
                        List<ListElement> listElements = result.getListElements();
                        int position = 0;
                        for (ListElement listElement : listElements) {
                            if (listElement instanceof Comment) {
                                Comment c = (Comment) listElement;
                                if (c.anchor.compareTo(anchor) >= 0) {
                                    result.anchor = c;
                                    break;
                                }
                            }
                            position++;
                        }
                        if (listView != null) {
                            //listView.smoothScrollToPosition(position);
                            listView.setSelection(position);
                        }
                    } else {
                        if (oldposition >= 0 && listView != null) {
                            //listView.smoothScrollToPosition(oldposition);
                        }
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
            });
        });

    }

    LogviewFragment getCurrentFragment() {
        LogviewFragment fragment = fragments.get(mPosition);
        if (fragment == null) {
            Bundle args = new Bundle();
            args.putString("channel", channels.get(mPosition));
            fragment = (LogviewFragment) Fragment.instantiate(ShowActivity.this, LogviewFragment.class.getName(), args);
            fragments.put(mPosition, fragment);
        }
        return fragment;
    }

    public String getAnchor() {
        return mAnchor;
    }

    public void setAnchor(String anchor) {
        this.mAnchor = anchor;
    }

    /**
     * @return the selected date as ISO-String
     */
    private String getDateString() {
        if (mSelectedDate == null) {
            return "today";
        }
        return DateUtil.getDateString(mSelectedDate);
    }

    @Override
    public boolean onOptionsItemSelected(@Nonnull MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int itemId = item.getItemId();
        if (itemId == R.id.options) {
            Intent intent = new Intent(this, OptionsActivity.class);
            startActivity(intent);
            return true;
        }
        if (itemId == R.id.refresh) {
            loadData();
            return true;
        }
        if (itemId == R.id.choose_date) {
            if (mSelectedDate == null) {
                mSelectedDate = new SimpleDate();
            }
            Dialog dialog = new DatePickerDialog(ShowActivity.this,
                    new DatePickerDialog.OnDateSetListener() {
                        // Workaround for https://code.google.com/p/android/issues/detail?id=34860
                        boolean executed = false;

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            if (!executed) {
                                mSelectedDate.year = year;
                                mSelectedDate.month = monthOfYear;
                                mSelectedDate.day = dayOfMonth;
                                loadData();
                            }
                            executed = true;
                        }
                    }, mSelectedDate.year, mSelectedDate.month, mSelectedDate.day) {

            };

            dialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateDateText(String text) {
        if (mMenu != null) {
            MenuItem item = mMenu.findItem(R.id.choose_date);
            item.setTitle(text);
        }
    }

    @Override
    public final boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        this.mMenu = menu;
        updateDateText(DateUtil.getDateString(new Date()));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.choose_date).setVisible(!drawerOpen);
        menu.findItem(R.id.refresh).setVisible(!drawerOpen);
        menu.findItem(R.id.options).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (mSelectedDate != null) {
            savedInstanceState.putString(Constants.STATE_DATE, DateUtil.getDateString(mSelectedDate));
        }
    }

    @Override
    protected void onRestoreInstanceState(@Nonnull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(Constants.STATE_DATE)) {
            mSelectedDate = new SimpleDate(DateUtil.getDate(savedInstanceState.getString(Constants.STATE_DATE)));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        String[] intentChannels = getIntent().getStringArrayExtra(Constants.INTENT_CHANNELS);
        channels = Arrays.asList(Objects.requireNonNullElse(intentChannels, Constants.DEFAULT_CHANNELS));

        DbOpenHelper.instance = new DbOpenHelper(this);

        mWatchesDao = new WatchesDao();
        mWatchesDao.open();

        setContentView(R.layout.activity_main);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerList = findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                R.layout.drawer_list_item, channels));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            setAnchor(extras.getString("anchor"));
            if (extras.containsKey(Constants.STATE_DATE)) {
                mSelectedDate = (SimpleDate) extras.getSerializable(Constants.STATE_DATE);
            }
            if (extras.containsKey("channel")) {
                setPosition(channels.indexOf(extras.getString("channel")));
            }
        }

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(channels.get(mPosition));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(R.string.app_name);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        setPosition(mPosition);

        if (savedInstanceState == null) {
            loadData();
        }
    }

    private void setPosition(int position) {
        if (mPosition != position) {
            mDrawerList.setItemChecked(mPosition, false);
        }
        mPosition = position;
        mDrawerList.setItemChecked(mPosition, true);
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            setPosition(position);
            mDrawerLayout.closeDrawer(mDrawerList);
            loadData();
        }
    }

    @Override
    protected void onResume() {
        mWatchesDao.open();
        super.onResume();

        //Load the list of usernames from the database that the application user is watching.
        mWatchList = mWatchesDao.getWatches();

        loadData();
    }

    @Override
    protected void onPause() {
        mWatchesDao.close();
        super.onPause();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
