/* OptionsActivity.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog;

import android.app.backup.BackupManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import de.egore911.drilog.db.WatchesDao;

/**
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.1
 */
public class OptionsActivity extends AppCompatActivity {

    private WatchesDao mWatchesDao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mWatchesDao = new WatchesDao();
        mWatchesDao.open();

        setContentView(R.layout.options);

        rebuildList();

        TextView watchField = findViewById(R.id.new_watch);
        watchField
                .setOnEditorActionListener((v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        addName(v);
                        return true;
                    }
                    return false;
                });

        Button addButton = findViewById(R.id.add);
        addButton.setOnClickListener(v -> addName(watchField));
    }

    @Override
    protected void onResume() {
        mWatchesDao.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mWatchesDao.close();
        super.onPause();
    }


    void addName(TextView watchField) {
        CharSequence text = watchField.getText();
        if (text != null) {
            mWatchesDao.addWatch(text.toString());
            BackupManager.dataChanged("de.egore911.drilog");
        }

        watchField.setText(null);

        rebuildList();
    }

    void rebuildList() {

        TextView noWatchesField = findViewById(R.id.no_watches);
        LinearLayout watchesLayout = findViewById(R.id.watches);

        List<String> watches = mWatchesDao.getWatches();

        watchesLayout.removeAllViews();

        if (watches.isEmpty()) {
            noWatchesField.setVisibility(View.VISIBLE);
            watchesLayout.setVisibility(View.GONE);
        } else {
            noWatchesField.setVisibility(View.GONE);
            watchesLayout.setVisibility(View.VISIBLE);
            for (String watch : watches) {
                addLine(watchesLayout, watch);
            }
        }
    }

    private void addLine(final LinearLayout watchesLayout, String watch) {
        RelativeLayout optionsLine = (RelativeLayout) getLayoutInflater()
                .inflate(R.layout.options_line, null);
        watchesLayout.addView(optionsLine);
        TextView watchEdit = optionsLine.findViewById(R.id.watch);
        watchEdit.setText(watch);
        Button removeButton = optionsLine.findViewById(R.id.remove);
        removeButton.setOnClickListener(v -> {
            RelativeLayout optionsLine1 = (RelativeLayout) v.getParent();
            TextView watchEdit1 = optionsLine1
                    .findViewById(R.id.watch);
            mWatchesDao.removeWatch(watchEdit1.getText().toString());
            BackupManager.dataChanged("de.egore911.drilog");

            rebuildList();
        });
    }
}
