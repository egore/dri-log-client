/* ShowDataAdapter.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog.adapter;

import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import de.egore911.drilog.R;
import de.egore911.drilog.ShowActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

import de.egore911.drilog.model.Comment;
import de.egore911.drilog.model.ListElement;
import de.egore911.drilog.model.User;
import de.egore911.drilog.util.DateUtil;

/**
 * This class is capable of rendering the elements of a ListView. The Lines to
 * be show must implement {@link ListElement}.
 *
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.1
 */
public class ShowDataAdapter extends BaseAdapter {

    public static class UserViewHolder {
        TextView nameField;
        ImageView avatarField;
        public User user;
    }

    public static class CommentViewHolder {
        TextView commentField;
        TextView dateField;
        public Comment comment;
    }

    private static final int TYPE_NAME = 0x0;
    private static final int TYPE_COMMENT = 0x1;

    private final LayoutInflater layoutInflater;
    protected Set<String> watchList;

    protected List<ListElement> listElements;

    public Date maxDate;
    public String channel;
    public Date date;
    public Comment anchor;

    public ShowDataAdapter(ShowActivity activity,
                           JSONObject o, Set<String> watchList) {
        // Store references to some of the Activities members (for using them
        // later)
        this.layoutInflater = activity.getLayoutInflater();

        updateElements(o, watchList);
    }

    public void updateElements(JSONObject o, Set<String> watchList) {
        this.watchList = watchList;
        // Prepare the data
        this.listElements = new ArrayList<>();
        if (o != null) {
            try {
                // Read some static data from the JSON-Structure
                channel = o.getString("channel");
                date = DateUtil.getDate(o.getString("date"));
                maxDate = DateUtil.getDate(o.getString("maxdate"));

                Map<String, User> userMap = new HashMap<>();
                // Now let's read the users
                JSONArray users = o.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject user = users.getJSONObject(i);
                    User u = User.create(user);
                    userMap.put(user.getString("name"), u);
                }

                // Now read the log lines
                User lastUser = null;
                JSONArray data = o.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    // Now let's add the log lines from this user
                    JSONObject comment = data.getJSONObject(i);
                    Comment c = Comment.create(comment);
                    User user = userMap.get(comment.getString("user"));
                    c.user = user;
                    if (lastUser != user) {
                        listElements.add(user);
                        lastUser = user;
                    }
                    listElements.add(c);
                }
            } catch (JSONException e) {
                Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listElements.size();
    }

    @Override
    public Object getItem(int position) {
        return listElements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Object o = getItem(position);
        boolean isUser = o instanceof User;

        ViewGroup row;
        if (null == convertView) {
            // Create a new view for the user or comment
            if (isUser) {
                row = (RelativeLayout) layoutInflater.inflate(
                        R.layout.line_name, null);
                UserViewHolder holder = new UserViewHolder();
                holder.nameField = row.findViewById(R.id.name);
                holder.avatarField = row.findViewById(R.id.avatar);
                holder.user = (User) o;
                row.setTag(holder);
            } else {
                row = (RelativeLayout) layoutInflater.inflate(
                        R.layout.line_comment, null);
                CommentViewHolder holder = new CommentViewHolder();
                holder.commentField = row.findViewById(R.id.comment);
                holder.dateField = row.findViewById(R.id.date);
                holder.comment = (Comment) o;
                row.setTag(holder);
            }
        } else {
            // Recycle a previous view if possible
            row = (ViewGroup) convertView;
            if (isUser) {
                UserViewHolder holder = (UserViewHolder) row.getTag();
                holder.user = (User) o;
            } else {
                CommentViewHolder holder = (CommentViewHolder) row.getTag();
                holder.comment = (Comment) o;
            }
        }

        if (isUser) {
            User user = (User) o;

            setDefaultCellBackground(position, row);

            // Render a user
            UserViewHolder holder = (UserViewHolder) row.getTag();
            if (watchList != null && watchList.contains(user.nick)) {
                holder.nameField.setTypeface(null, Typeface.BOLD);
            } else {
                holder.nameField.setTypeface(null, Typeface.NORMAL);
            }
            holder.nameField.setText(user.name);
            Picasso.get()
                    .load(user.image)
                    .placeholder(R.drawable.ic_launcher)
                    .into(holder.avatarField);
        } else {
            Comment comment = (Comment) o;

            setDefaultCellBackground(position, row);

            // Render a comment
            CommentViewHolder holder = (CommentViewHolder) row.getTag();
            holder.commentField.setText(comment.content);
            holder.dateField.setText(comment.time);
        }

        return row;
    }

    @Override
    public int getViewTypeCount() {
        // We got two types of lines: users and comments
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof User) {
            return TYPE_NAME;
        }
        return TYPE_COMMENT;
    }

    public List<ListElement> getListElements() {
        return listElements;
    }

    private void setDefaultCellBackground(int position, ViewGroup row) {
        if (getCount() == 0) {
            // Only one element -> border at top and bottom
            int drawable = R.drawable.table_shape_both;
            row.setBackgroundResource(drawable);
        } else if (getItemViewType(position) == TYPE_NAME) {
            // User -> border at top
            int drawable = R.drawable.table_shape_top;
            row.setBackgroundResource(drawable);
        } else if (position == (getCount() - 1)
                || (getItem(position + 1) instanceof User)) {
            // Last element or next element is a user -> border at bottom
            int drawable = R.drawable.table_shape_bottom;
            row.setBackgroundResource(drawable);
        } else {
            // Nothing special, border at the left and right
            int drawable = R.drawable.table_shape_middle;
            row.setBackgroundResource(drawable);
        }
    }

    public void setWatchList(Set<String> watchList) {
        this.watchList = watchList;
    }

}
