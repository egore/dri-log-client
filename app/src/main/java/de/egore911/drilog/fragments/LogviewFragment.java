/* LogviewFragment.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import de.egore911.drilog.R;
import de.egore911.drilog.ShowActivity;
import de.egore911.drilog.adapter.ShowDataAdapter;

import java.util.HashSet;
import java.util.List;

/**
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.3
 */
public class LogviewFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_logview, container,
                false);
        ListView list = layout.findViewById(android.R.id.list);
        list.setEmptyView(layout.findViewById(android.R.id.empty));
        return layout;
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        ViewGroup row = (ViewGroup) v;
        Object tag = row.getTag();

        if (tag instanceof ShowDataAdapter.UserViewHolder) {

            ShowDataAdapter.UserViewHolder u = (ShowDataAdapter.UserViewHolder) tag;

            List<String> current = ((ShowActivity) getActivity()).mWatchesDao.getWatches();

            String nick = u.user.nick;
            if (current.contains(nick)) {
                ((ShowActivity) getActivity()).mWatchesDao.removeWatch(nick);
                current.remove(nick);
            } else {
                ((ShowActivity) getActivity()).mWatchesDao.addWatch(nick);
                current.add(nick);
            }

            ShowDataAdapter listAdapter = (ShowDataAdapter) getListAdapter();
            listAdapter.setWatchList(new HashSet<>(current));
            // TODO this causes the views to be recycled which causes a flicker of the images
            listAdapter.notifyDataSetChanged();
        }
    }

}
